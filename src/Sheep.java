import java.util.ArrayList;

public class Sheep {

   public static int right = 0;
   public static int left = 0;
   public static int bottom = 0;
   public static int top = 0;


   public static void main (String[] param) {
      int n = 4;
      int[][] a = { { 1, 2, 3, 4 },
                    { 5, 6, 7, 8 },
                    { 1, 2, 3, 4 },
                    { 5, 6, 7, 8 },
                    {9 ,8 ,7 , 2} };

      int[][] b = { { 1, 2, 3, 4 },
                    { 5, 6, 7, 8 } };

      int[][] c = {  { 1, 2, 3, 4 },
                     { 5, 6, 7, 8 },
                     {1, 2, 3, 4},
                     {8, 5, 6, 3} };


      bouncingDiagonal(a, n); // [1, 6, 3, 8, 7, 6, 1]
      bouncingDiagonal(b, n); // [1, 6, 3, 8, 3, 6, 1]
      bouncingDiagonal(c, n); // [1, 6, 3, 3, 3, 6, 1]

   }


   static void bouncingDiagonal(int[][] mat, int n)
   {
      right = mat[0].length - 1;
      left = 0;
      bottom = mat.length - 1;
      top = 0;

      ArrayList<Integer> list = new ArrayList();
      ArrayList<Integer> list2 = new ArrayList();

      list.add(mat[0][0]);
      list.add(mat[1][1]);
      int rida = 1;
      int veerg = 1;
      int previousRida = 0;
      int previousVeerg = 0;

      while (veerg != 0) {
         // kui mxm
         if(rida == 0 && veerg == right || rida == bottom && veerg == right) {
            for (int i = list.size() - 1; i >= 0; i--) {
               list2.add(list.get(i));
            }
            list2.remove(0);
            list.addAll(list2);
            break;
         }
         try {
            if(previousVeerg < veerg) {
               list.add(mat[rida + 1][veerg + 1]);
               rida++;
               veerg++;
               previousRida++;
               previousVeerg++;
            } else{
               list.add(mat[rida - 1][veerg - 1]);
               rida--;
               veerg--;
               previousRida--;
               previousVeerg--;
            }

         }catch (Exception e) {
            if(rida == bottom) {
               if(previousVeerg < veerg) {
                  rida--;
                  veerg++;
                  list.add(mat[rida][veerg]);
                  previousVeerg++;
                  previousRida++;

               } else if(previousVeerg > veerg){
                  rida--;
                  veerg--;
                  list.add(mat[rida][veerg]);
                  previousVeerg--;
                  previousRida++;
               }
               
            } else if(veerg == right){
               if(previousRida < rida) {
                  rida++;
                  veerg--;
                  list.add(mat[rida][veerg]);
                  previousVeerg++;
                  previousRida++;
               } else if(previousRida > rida) {
                  rida--;
                  veerg--;
                  list.add(mat[rida][veerg]);
                  previousRida--;
                  previousVeerg++;

               }

            } else if(rida == top){
               if(previousVeerg < veerg) {
                  rida++;
                  veerg++;
                  list.add(mat[rida][veerg]);
                  previousVeerg++;
                  previousRida--;
               } else if(previousVeerg > veerg) {
                  rida--;
                  veerg--;
                  list.add(mat[rida][veerg]);
                  previousVeerg--;
                  previousRida--;
               }

            }
         }
      }

      System.out.println(list);
   }
}

